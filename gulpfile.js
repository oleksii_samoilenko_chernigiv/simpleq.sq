var gulp 	 	     = require('gulp'),
		sassToCss 	 = require('gulp-sass'),
		concat       = require("gulp-concat"),
		uglifyjs     = require("gulp-uglify"),
		uglifycss    = require("gulp-uglifycss"),
		autoprefixer = require("gulp-autoprefixer"),
		rename       = require("gulp-rename"),
		browserSync  = require('browser-sync');

//Compile CSS file from Sass
gulp.task('compileSass', function(){
	return gulp.src([
			'app/sass/**/*.{sass,scss}'
		])
	.pipe(sassToCss({outputStyle: 'expanded'}).on('error', sassToCss.logError))
	.pipe(autoprefixer(['last 15 versions', '> 1%', 'ie 8', 'ie 7'], {cascade: true}))
	.pipe(uglifycss({
      "maxLineLen": 80,
      "uglyComments": true
    }))
	.pipe(gulp.dest('app/css'))
	.pipe(browserSync.reload({stream: true}));
});

//Concat all library js files into libs.min.js
gulp.task('createJsLibs', function(){
	return gulp.src([
			'app/libs/jquery/dist/jquery.min.js',
			'app/libs/slick-carousel/slick.js',
			'app/libs/semantic/dist/components/dropdown.js',
			'app/libs/semantic/dist/components/transition.js',
			// 'app/libs/semantic/dist/components/modal.js',
			// 'app/libs/semantic/dist/components/dimmer.js',
			// 'app/libs/semantic/dist/components/popup.js',
			// 'app/libs/waypoints/lib/jquery.waypoints.js',
			// 'app/libs/jquery-ui/ui/widget.js',
			// 'app/libs/jquery-ui/ui/unique-id.js',
			// 'app/libs/jquery-ui/ui/widgets/tabs.js',
			// 'app/libs/easings/easing.min.js',
			// 'app/libs/jquery.maskedinput-master/dist/jquery.maskedinput.js'
		])
	.pipe(concat('libs.min.js'))
	.pipe(uglifyjs())
	.pipe(gulp.dest('app/js'))
	.pipe(browserSync.reload({stream: true}));
});

//Compress JSFiles
gulp.task('compressJs', function(){
	return gulp.src([
			'app/js_uncompress/*.js'
		])
	.pipe(uglifyjs())
	.pipe(rename({
		suffix: ".min"
	}))
	.pipe(gulp.dest('app/js'))
	.pipe(browserSync.reload({stream: true}));
});

gulp.task('code', function(){
	return gulp.src([
			'app/**/*.{php, html}'
		])
	.pipe(browserSync.reload({stream: true}));
});

// LiveReload
gulp.task('browserSync', function serverStart(){
	browserSync({
		proxy: "simpleq.sq"
	});
});

// Watch
gulp.task('watch', gulp.parallel('code', 'compileSass', 'createJsLibs', 'compressJs', 'browserSync', function startWatching(){
	gulp.watch('app/sass/**/*.{css,sass,scss}', gulp.parallel('compileSass'));
	gulp.watch('app/js_uncompress/**/*.js', gulp.parallel('compressJs'));
	gulp.watch('app/**/*.{php,html}', gulp.parallel('code'));
}));

// Default Gulp function
gulp.task('default', gulp.parallel('watch'));