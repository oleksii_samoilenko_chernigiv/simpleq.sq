<!DOCTYPE html>
<html lang="ru">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <meta name="HandheldFriendly" content="true">

  <meta name="description" content="This is description">
  <meta name="keywords" content="keywords">
  <title>SimpleQ</title>
  <link rel="shortcut icon" href="img/favicon.jpg" type="image/x-icon">
  <link rel="icon" href="img/favicon.jpg" type="image/x-icon">

  <style>
    #page-preloader {
      position: fixed;
      left: 0;
      top: 0;
      right: 0;
      bottom: 0;
      background: #fff;
      z-index: 100500;
    }
    #page-preloader .spinner {
      width: 300px;
      height: 99px;
      position: absolute;
      left: 50%;
      top: 50%;
      margin: -50px 0 0 -150px;
      background: url('img/logo.png') no-repeat 50% 50%;
      background-size: contain;
      -webkit-animation-duration: 1s;
      animation-duration: 1s;
      -webkit-animation-fill-mode: both;
      animation-fill-mode: both;
    }
    @-webkit-keyframes preloader-anim {
      0% {
        opacity: 0;
        -webkit-transform: translateY(300px);
        transform: translateY(300px);
      }

      100% {
        opacity: 1;
        -webkit-transform: translateY(0);
        transform: translateY(0);
      }
    }
    @keyframes preloader-anim {
      0% {
        opacity: 0;
        -webkit-transform: translateY(300px);
        -ms-transform: translateY(300px);
        transform: translateY(300px);
      }

      100% {
        opacity: 1;
        -webkit-transform: translateY(0);
        -ms-transform: translateY(0);
        transform: translateY(0);
      }
    }
    .preloader-anim {
      -webkit-animation-name: preloader-anim;
      animation-name: preloader-anim;
    }
  </style>

  <!-- Styles -->
    <!-- Libs -->
      <link rel="stylesheet" type="text/css" href="css/libs.css">
    <!-- Common -->
      <link rel="stylesheet" type="text/css" href="css/common.css">
    <!-- Custom -->
      <link rel="stylesheet" type="text/css" href="css/index.css">

  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body>
  
  <!-- START Preloader -->
    <div id="page-preloader"><span class="spinner preloader-anim"></span></div>
  <!-- Finish Preloader -->

  <?php
    include 'php-components/header.php';
  ?>
  
  <main class="main">
    <div id="cover" class="cover">
      <div class="container">
        <h1>
          Simple<span class="red">Q</span>
        </h1>
        <p class="cover-subtitle">
          Digitale Lösungen für die Immobilienbranche
        </p>
        <div class="cover-flex">
          <div class="item vermietung">
            <div class="icon-container">
              <img src="img/icons/vermietung.svg" alt="img">
            </div>
            <a href="rent.php" class="button">
              Vermietung
            </a>
          </div>
          <div class="item verwaltung">
            <div class="icon-container">
              <img src="img/icons/verwaltung.svg" alt="img">
            </div>
            <a href="facility.php" class="button">
              Verwaltung
            </a>
          </div>
          <div class="item verkauf">
            <div class="icon-container">
              <img src="img/icons/verkauf.svg" alt="img">
            </div>
            <a href="sales.php" class="button">
              Verkauf
            </a>
          </div>
        </div>
      </div>
    </div>
    <div class="uber_uns">
      <div class="container">
        <div class="text-with-left-icon">
          <div class="left-icon">
            <div class="icon-container">
              <img src="img/icons/for-uber_uns.svg" alt="icon">
              <p>Über uns</p>
            </div>
          </div>
          <div class="right-text">
            <p>
              Die SimpleQ GmbH ist ein in Zürich ansässiges PropTech-Unternehmen, das IT-und Beratungsdienstleistungen anbietet. Hinter der SimpleQ GmbH steht eine Gruppe von Unternehmern, die leidenschaftlich an die Digitalisierung der Immobilienwirtschaft glauben.
              <br>
              <br>
              Die SimpleQ-Plattform ist eine Softwarelösung, die in der Schweiz gehostet ist und deren Daten die Schweiz nicht verlassen.
            </p>
          </div>
        </div>
      </div>
    </div>
    <div id="team" class="team">
      <div class="container">
        <div class="icon-container">
          <img src="img/icons/for-team.svg" alt="icon">
        </div>
        <h4>Unser Management-Team</h4>
        <div class="team-flex">
          <div class="member">
            <div class="member-photo">
              <img src="img/team/1.png" alt="photo">
            </div>
            <div class="member-descr">
              <p class="name">Dr. Irina Starobinskaya, FRM</p>
              <p class="position">CEO &amp; Co-founder</p>
              <ul>
                <li>
                  <span class="red">•</span> Über 10 Jahre Erfahrung in Unternehmensstrategie und strategischer Beratung
                </li>
                <li>
                  <span class="red">•</span> Bevor SimpleQ war Marina als Director of Corporated Strategy bei Flex Ltd. tätig, einem globalen Unternehmen mit einem Umsatz von 26 Milliarden Dollar, davor bei der Boston Consulting Group (BCG)
                </li>
                <li>
                  <span class="red">•</span> Doktortitel in Finance des Swiss Finance Institute
                </li>
              </ul>
            </div>
          </div>
          <div class="member">
            <div class="member-photo">
              <img src="img/team/2.png" alt="photo">
            </div>
            <div class="member-descr">
              <p class="name">Dr. Marina Druz</p>
              <p class="position">CEO &amp; Co-founder</p>
              <ul>
                <li>
                  <span class="red">•</span> Über 10 Jahre Erfahrung in Unternehmensstrategie und strategischer Beratung
                </li>
                <li>
                  <span class="red">•</span> Bevor SimpleQ war Marina als Director of Corporated Strategy bei Flex Ltd. tätig, einem globalen Unternehmen mit einem Umsatz von 26 Milliarden Dollar, davor bei der Boston Consulting Group (BCG)
                </li>
                <li>
                  <span class="red">•</span> Doktortitel in Finance des Swiss Finance Institute
                </li>
              </ul>
            </div>
          </div>
          <!-- <div class="member">
            <div class="member-photo">
              <img src="img/team/3.png" alt="photo">
            </div>
            <div class="member-descr">
              <p class="name">Dr. Irina Starobinskaya, FRM</p>
              <p class="position">CEO &amp; Co-founder</p>
              <ul>
                <li>
                  <span class="red">•</span> Über 10 Jahre Erfahrung in Unternehmensstrategie und strategischer Beratung
                </li>
                <li>
                  <span class="red">•</span> Bevor SimpleQ war Marina als Director of Corporated Strategy bei Flex Ltd. tätig, einem globalen Unternehmen mit einem Umsatz von 26 Milliarden Dollar, davor bei der Boston Consulting Group (BCG)
                </li>
                <li>
                  <span class="red">•</span> Doktortitel in Finance des Swiss Finance Institute
                </li>
              </ul>
            </div>
          </div>
          <div class="member">
            <div class="member-photo">
              <img src="img/team/4.png" alt="photo">
            </div>
            <div class="member-descr">
              <p class="name">Dr. Irina Starobinskaya, FRM</p>
              <p class="position">CEO &amp; Co-founder</p>
              <ul>
                <li>
                  <span class="red">•</span> Über 10 Jahre Erfahrung in Unternehmensstrategie und strategischer Beratung
                </li>
                <li>
                  <span class="red">•</span> Bevor SimpleQ war Marina als Director of Corporated Strategy bei Flex Ltd. tätig, einem globalen Unternehmen mit einem Umsatz von 26 Milliarden Dollar, davor bei der Boston Consulting Group (BCG)
                </li>
                <li>
                  <span class="red">•</span> Doktortitel in Finance des Swiss Finance Institute
                </li>
              </ul>
            </div>
          </div> -->
        </div>
      </div>
    </div>
    <div id="slider-block" class="slider-block">
      <div class="container">
        <div class="icon-container">
          <img src="img/icons/for-slider.svg" alt="icon">
        </div>
        <h4>SimpleQ Beratungsdienstleistungen</h4>
        <p class="slider-subtitle">Unsere Beratungsprojekte</p>
      </div>
      <div class="container">
        <div class="slider">
          <div class="slide" style="background-image: url('img/slider/1.jpg');">
              <div class="slide-info">
                <p>
                  <span>Kunde:</span>
                  Immobilienverwaltung
                </p>
                <p>
                  <span>Herausforderung:</span>
                  Hohe Kosten für den Geschäftsbetrieb
                </p>
                <p>
                  <span>Projekt:</span>
                  1 Identifizierung des Ersparnispotenzials
                </p>
              </div>
          </div>
          <div class="slide" style="background-image: url('img/slider/2.jpg');">
              <div class="slide-info">
                <p>
                  <span>Kunde:</span>
                  Immobilienverwaltung
                </p>
                <p>
                  <span>Herausforderung:</span>
                  Hohe Kosten für den Geschäftsbetrieb
                </p>
                <p>
                  <span>Projekt:</span>
                  2 Identifizierung des Ersparnispotenzials
                </p>
              </div>
          </div>
          <div class="slide active" style="background-image: url('img/slider/3.jpg');">
              <div class="slide-info">
                <p>
                  <span>Kunde:</span>
                  Immobilienverwaltung
                </p>
                <p>
                  <span>Herausforderung:</span>
                  Hohe Kosten für den Geschäftsbetrieb
                </p>
                <p>
                  <span>Projekt:</span>
                  3 Identifizierung des Ersparnispotenzials
                </p>
              </div>
          </div>
          <div class="slide" style="background-image: url('img/slider/4.jpg');">
              <div class="slide-info">
                <p>
                  <span>Kunde:</span>
                  Immobilienverwaltung
                </p>
                <p>
                  <span>Herausforderung:</span>
                  Hohe Kosten für den Geschäftsbetrieb
                </p>
                <p>
                  <span>Projekt:</span>
                  4 Identifizierung des Ersparnispotenzials
                </p>
              </div>
          </div>
          <div class="slide" style="background-image: url('img/slider/5.jpg');">
              <div class="slide-info">
                <p>
                  <span>Kunde:</span>
                  Immobilienverwaltung
                </p>
                <p>
                  <span>Herausforderung:</span>
                  Hohe Kosten für den Geschäftsbetrieb
                </p>
                <p>
                  <span>Projekt:</span>
                  5 Identifizierung des Ersparnispotenzials
                </p>
              </div>
          </div>
        </div>
      </div>
      <div class="container">
        <p class="slider-subtitle">Gerne helfen wir auch Ihrem Unternehmen, von Digitalisierung und umsetzungsfähiger Analytik zu profitieren.</p>
        <a href="#" class="button">Vereinbaren Sie heute ein <br>kostenloses Beratungsgespräch</a>
      </div>
    </div>
    <?php
      include 'php-components/contacts.php';
    ?>
  </main>
    
  <?php
    include 'php-components/footer.php';
  ?>
  
  <!-- Scripts -->
    <!-- Libs -->
      <script defer src="js/libs.min.js"></script>
    <!-- Common -->
      <script defer src="js/common.min.js"></script>
    <!-- Custom -->
      <script defer src="js/index.min.js"></script>
</body>
</html>