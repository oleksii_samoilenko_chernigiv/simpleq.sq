$(document).ready(function() {

	// Initialization about us dropdown
		$('.ui.dropdown.about_us').dropdown({
			action: 'select'
		});

	// Initialization language dropdown
		$('.ui.dropdown.language').dropdown();

	// Open/close header-menu-popup
		$('.header-menu-link p').click(function(e) {
      e.preventDefault();
      $('.header-menu-link').removeClass('active');
      $( this ).closest('.header-menu-link').addClass('active');
    });
		$('.close-header-menu-popup').click(function(e) {
      e.preventDefault();
      $( this ).closest('.header-menu-link').removeClass('active');
    });
});