<!DOCTYPE html>
<html lang="ru">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <meta name="HandheldFriendly" content="true">

  <meta name="description" content="This is description">
  <meta name="keywords" content="keywords">
  <title>SimpleQ</title>
  <link rel="shortcut icon" href="img/favicon.jpg" type="image/x-icon">
  <link rel="icon" href="img/favicon.jpg" type="image/x-icon">

  <!-- Styles -->
    <!-- Libs -->
      <link rel="stylesheet" type="text/css" href="css/libs.css">
    <!-- Common -->
      <link rel="stylesheet" type="text/css" href="css/common.css">
    <!-- Custom -->
      <link rel="stylesheet" type="text/css" href="css/inner.css">

  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="inner">
  <?php
    include 'php-components/header.php';
  ?>
  
  <main class="main">
    <div class="cover">
      <div class="container">
        <div class="title-wrapper rent">
          <div class="icon-container">
            <img src="img/icons/vermietung.svg" alt="icon">
          </div>
          <h1>SimpleQ Plattform für<br>Immobilienvermietung</h1>
        </div>
        <div class="cover-item rent-cover-item-1">
          <div class="img-wrapper">
            <img class="screen" src="img/inner-images/macbook-and-iphone.png" alt="image">
          </div>
          <div class="text-wrapper">
            <p class="text-wrapper-title">
              Mit Online-Mietanträgen lernen Sie potenzielle Mieter besser kennen
            </p>
            <ul>
              <li>
                <svg xmlns="http://www.w3.org/2000/svg" width="7.969" height="15" viewBox="0 0 7.969 15">
                  <path d="M863,729.989V717.01a0.938,0.938,0,0,1,1.612-.714l6.075,6.49a1.06,1.06,0,0,1,0,1.427l-6.075,6.489A0.938,0.938,0,0,1,863,729.989Z" transform="translate(-863 -716)"/>
                </svg>
                Online-Bewerbungsformular verfügbar auf allen Geräten über einen individuellen Link (URL) oder QR-Code.
              </li>
              <li>
                <svg xmlns="http://www.w3.org/2000/svg" width="7.969" height="15" viewBox="0 0 7.969 15">
                  <path d="M863,729.989V717.01a0.938,0.938,0,0,1,1.612-.714l6.075,6.49a1.06,1.06,0,0,1,0,1.427l-6.075,6.489A0.938,0.938,0,0,1,863,729.989Z" transform="translate(-863 -716)"/>
                </svg>
                Vermieter-Dashboard: Sie behalten jederzeit den Überblick über allen Bewerbungen.
              </li>
              <li>
                <svg xmlns="http://www.w3.org/2000/svg" width="7.969" height="15" viewBox="0 0 7.969 15">
                  <path d="M863,729.989V717.01a0.938,0.938,0,0,1,1.612-.714l6.075,6.49a1.06,1.06,0,0,1,0,1.427l-6.075,6.489A0.938,0.938,0,0,1,863,729.989Z" transform="translate(-863 -716)"/>
                </svg>
                Verwaltung von den Wartelisten und automatischer Abgleich mit den freien Objekten. 
              </li>
            </ul>
          </div>
        </div>
        <div class="cover-item rent-cover-item-2">
          <div class="text-wrapper">
            <p class="text-wrapper-title">
              Erhalten Sie die Betreibungsauszüge direkt mit der kostenlosen Schnittstelle zu den Betreibungsämtern
            </p>
            <ul>
              <li>
                <svg xmlns="http://www.w3.org/2000/svg" width="7.969" height="15" viewBox="0 0 7.969 15">
                  <path d="M863,729.989V717.01a0.938,0.938,0,0,1,1.612-.714l6.075,6.49a1.06,1.06,0,0,1,0,1.427l-6.075,6.489A0.938,0.938,0,0,1,863,729.989Z" transform="translate(-863 -716)"/>
                </svg>
                Kostenloser*  Zugriff auf die Betreibungsauszüge. Minimiertes Risiko der Berichtfälschungen.
              </li>
              <li>
                <svg xmlns="http://www.w3.org/2000/svg" width="7.969" height="15" viewBox="0 0 7.969 15">
                  <path d="M863,729.989V717.01a0.938,0.938,0,0,1,1.612-.714l6.075,6.49a1.06,1.06,0,0,1,0,1.427l-6.075,6.489A0.938,0.938,0,0,1,863,729.989Z" transform="translate(-863 -716)"/>
                </svg>
                Elektronische Zustellung der Betreibungsauszüge an die Bewerben und Verwaltung innerhalb von 24 Stunden**
              </li>
              <li>
                <svg xmlns="http://www.w3.org/2000/svg" width="7.969" height="15" viewBox="0 0 7.969 15">
                  <path d="M863,729.989V717.01a0.938,0.938,0,0,1,1.612-.714l6.075,6.49a1.06,1.06,0,0,1,0,1.427l-6.075,6.489A0.938,0.938,0,0,1,863,729.989Z" transform="translate(-863 -716)"/>
                </svg>
                Automatische Auswertung der Betreibungsauszüge und Empfehlungen für die Mieterauswahl.
              </li>
            </ul>
            <p class="red bold">
              *Für Vermieter/Verwaltung
              <br>
              **Keine rechtliche Garantie, es handelt sich nur um allgemeinen Zeitrahmen
            </p>
          </div>
          <div class="img-wrapper">
            <img class="screen" src="img/inner-images/rent-screen-2.png" alt="image">
          </div>
        </div>
        <div class="cover-item rent-cover-item-3">
          <div class="img-wrapper">
            <img class="screen" src="img/inner-images/facility-screen-3.png" alt="image">
          </div>
          <div class="text-wrapper">
            <p class="text-wrapper-title">
              Finden Sie einen Mieter, der pünktlich bezahlt und langfristig im Mietobjekt bleibt - mit SimpleQ Scoring
            </p>
            <ul>
              <li>
                <svg xmlns="http://www.w3.org/2000/svg" width="7.969" height="15" viewBox="0 0 7.969 15">
                  <path d="M863,729.989V717.01a0.938,0.938,0,0,1,1.612-.714l6.075,6.49a1.06,1.06,0,0,1,0,1.427l-6.075,6.489A0.938,0.938,0,0,1,863,729.989Z" transform="translate(-863 -716)"/>
                </svg>
                Statistisch-basiertes Mieterscoring als Grundlage für Ihre Entscheidung.
              </li>
              <li>
                <svg xmlns="http://www.w3.org/2000/svg" width="7.969" height="15" viewBox="0 0 7.969 15">
                  <path d="M863,729.989V717.01a0.938,0.938,0,0,1,1.612-.714l6.075,6.49a1.06,1.06,0,0,1,0,1.427l-6.075,6.489A0.938,0.938,0,0,1,863,729.989Z" transform="translate(-863 -716)"/>
                </svg>
                Verifizierte Informationen über die Bewerber: Einkommenseinschätzung, Schulden, Kreditrisiken usw.
              </li>
              <li>
                <svg xmlns="http://www.w3.org/2000/svg" width="7.969" height="15" viewBox="0 0 7.969 15">
                  <path d="M863,729.989V717.01a0.938,0.938,0,0,1,1.612-.714l6.075,6.49a1.06,1.06,0,0,1,0,1.427l-6.075,6.489A0.938,0.938,0,0,1,863,729.989Z" transform="translate(-863 -716)"/>
                </svg>
                Die Entscheidung, ob der Bewerber die Kriterien für Ihre Anmietung erfüllt, bleibt bei Ihnen.
              </li>
            </ul>
          </div>
        </div>
        <div class="cover-item rent-cover-item-4 width-50-percent">
          <div class="text-wrapper">
            <p class="text-wrapper-title">
              Geben Sie den Bewohner Online-Zugang zu wichtigen Informationen - mit dem SimpleQ Mieterportal
            </p>
            <ul>
              <li>
                <svg xmlns="http://www.w3.org/2000/svg" width="7.969" height="15" viewBox="0 0 7.969 15">
                  <path d="M863,729.989V717.01a0.938,0.938,0,0,1,1.612-.714l6.075,6.49a1.06,1.06,0,0,1,0,1.427l-6.075,6.489A0.938,0.938,0,0,1,863,729.989Z" transform="translate(-863 -716)"/>
                </svg>
                Sicherer, sofortiger Zugriff auf die wichtigen Informationen für die Bewohnern.
              </li>
              <li>
                <svg xmlns="http://www.w3.org/2000/svg" width="7.969" height="15" viewBox="0 0 7.969 15">
                  <path d="M863,729.989V717.01a0.938,0.938,0,0,1,1.612-.714l6.075,6.49a1.06,1.06,0,0,1,0,1.427l-6.075,6.489A0.938,0.938,0,0,1,863,729.989Z" transform="translate(-863 -716)"/>
                </svg>
                Bequemer Kommunikationsweg mit den Bewohnern.
              </li>
              <li>
                <svg xmlns="http://www.w3.org/2000/svg" width="7.969" height="15" viewBox="0 0 7.969 15">
                  <path d="M863,729.989V717.01a0.938,0.938,0,0,1,1.612-.714l6.075,6.49a1.06,1.06,0,0,1,0,1.427l-6.075,6.489A0.938,0.938,0,0,1,863,729.989Z" transform="translate(-863 -716)"/>
                </svg>
                Digitales Schwarzes Brett.
              </li>
            </ul>
          </div>
          <div class="img-wrapper">
            <img class="screen" src="img/inner-images/imac.png" alt="image">
          </div>
        </div>
        <div class="cover-item rent-cover-item-5">
          <div class="img-wrapper">
            <img class="screen" src="img/inner-images/rent-screen-5.png" alt="image">
          </div>
          <div class="text-wrapper">
            <p class="text-wrapper-title">
              Sparen Sie Zeit und Mühe durch die transparente Schaden- und Auftragsmeldung
            </p>
            <ul>
              <li>
                <svg xmlns="http://www.w3.org/2000/svg" width="7.969" height="15" viewBox="0 0 7.969 15">
                  <path d="M863,729.989V717.01a0.938,0.938,0,0,1,1.612-.714l6.075,6.49a1.06,1.06,0,0,1,0,1.427l-6.075,6.489A0.938,0.938,0,0,1,863,729.989Z" transform="translate(-863 -716)"/>
                </svg>
                Digitale Wartungsanfragen.
              </li>
              <li>
                <svg xmlns="http://www.w3.org/2000/svg" width="7.969" height="15" viewBox="0 0 7.969 15">
                  <path d="M863,729.989V717.01a0.938,0.938,0,0,1,1.612-.714l6.075,6.49a1.06,1.06,0,0,1,0,1.427l-6.075,6.489A0.938,0.938,0,0,1,863,729.989Z" transform="translate(-863 -716)"/>
                </svg>
                Kalender- und Terminplanungstools mit Status-Updates in Echtzeit.
              </li>
              <li>
                <svg xmlns="http://www.w3.org/2000/svg" width="7.969" height="15" viewBox="0 0 7.969 15">
                  <path d="M863,729.989V717.01a0.938,0.938,0,0,1,1.612-.714l6.075,6.49a1.06,1.06,0,0,1,0,1.427l-6.075,6.489A0.938,0.938,0,0,1,863,729.989Z" transform="translate(-863 -716)"/>
                </svg>
                Datenbank der Instandhaltungsdienstleister Ihres Vertrauens. Integration mit dem offiziellen Telefonbuch.
              </li>
            </ul>
          </div>
        </div>
        <div class="cover-item rent-cover-item-6">
          <div class="text-wrapper">
            <p class="text-wrapper-title">
              Rechtssichere elektronische Abnahme- und Übergabeprotokolle -  mit der digitalen Wohnungsübergabe von SimpleQ
            </p>
            <ul>
              <li>
                <svg xmlns="http://www.w3.org/2000/svg" width="7.969" height="15" viewBox="0 0 7.969 15">
                  <path d="M863,729.989V717.01a0.938,0.938,0,0,1,1.612-.714l6.075,6.49a1.06,1.06,0,0,1,0,1.427l-6.075,6.489A0.938,0.938,0,0,1,863,729.989Z" transform="translate(-863 -716)"/>
                </svg>
                Digitale Abnahme- und Übergabeprotokolle
              </li>
              <li>
                <svg xmlns="http://www.w3.org/2000/svg" width="7.969" height="15" viewBox="0 0 7.969 15">
                  <path d="M863,729.989V717.01a0.938,0.938,0,0,1,1.612-.714l6.075,6.49a1.06,1.06,0,0,1,0,1.427l-6.075,6.489A0.938,0.938,0,0,1,863,729.989Z" transform="translate(-863 -716)"/>
                </svg>
                Fotos von Mängeln können dem Abnahme- und Übergabeprotokoll direkt von der Kamera Ihres Geräts hinzugefügt werden.
              </li>
              <li>
                <svg xmlns="http://www.w3.org/2000/svg" width="7.969" height="15" viewBox="0 0 7.969 15">
                  <path d="M863,729.989V717.01a0.938,0.938,0,0,1,1.612-.714l6.075,6.49a1.06,1.06,0,0,1,0,1.427l-6.075,6.489A0.938,0.938,0,0,1,863,729.989Z" transform="translate(-863 -716)"/>
                </svg>
                Unterschreibung den Abnahme- und Übergabeprotokoll direkt auf dem mobilen Gerät.
              </li>
            </ul>
          </div>
          <div class="img-wrapper">
            <img class="screen" src="img/inner-images/rent-screen-2.png" alt="image">
          </div>
        </div>
      </div>
    </div>
    <div class="preise">
      <div class="container">
        <div class="icon-container">
          <img src="img/icons/for-preise.png" alt="icon">
          <p>Preise</p>
        </div>
        <p class="subtitle">Keine Einrichtungsgebühren oder Mindestvertragslaufzeit</p>
        <div class="preise-flex">
          <div class="item">
            <img src="img/icons/preise-icon.png" alt="icon">
            <p class="item-title">
              Vermietung
            </p>
            <ul>
              <li><span class="red">•</span> Online-Mietanträge</li>
              <li><span class="red">•</span> Kostenlose Schnittstelle zu den Betreibungsämtern</li>
              <li><span class="red">•</span> Mieterscoring für Mieterauswahl</li>
            </ul>
            <div class="price">
              <p class="red">20</p>
              <p class="price-descr">
                <span class="line-through">30</span> CHF
                <br>
                <span class="red">Monat/Objekt</span>
              </p>
            </div>
            <a href="#" class="button">
              14 Tage kostenlos testen
            </a>
          </div>
          <div class="item">
            <img src="img/icons/preise-icon-2.png" alt="icon">
            <p class="item-title">
              Vermietung &amp; <span class="info" title="Das Patket Verwaltung ist auch separat erhaltlich">Verwaltung</span>
            </p>
            <ul>
              <li><span class="red">•</span> Online-Mietanträge</li>
              <li><span class="red">•</span> Kostenlose Schnittstelle zu den Betreibungsämtern</li>
              <li><span class="red">•</span> Mieterscoring für Mieterauswahl</li>
              <li><span class="red">•</span> Online-Mieterportal</li>
              <li><span class="red">•</span> Digitale Schaden- und Auftragsmeldung</li>
              <li><span class="red">•</span> Digitale Abnahme- und Übergabeprotokolle</li>
            </ul>
            <div class="price">
              <p class="red">25</p>
              <p class="price-descr">
                <span class="line-through">40</span> CHF
                <br>
                <span class="red">Monat/Objekt</span>
              </p>
            </div>
            <a href="#" class="button">
              14 Tage kostenlos testen
            </a>
          </div>
          <div class="item">
            <img src="img/icons/preise-icon-3.png" alt="icon">
            <p class="item-title">
              Vermietung &amp; Verwaltung <span class="red">Premium</span>
            </p>
            <ul>
              <li><span class="red">•</span> Online-Mietanträge</li>
              <li><span class="red">•</span> Kostenlose Schnittstelle zu den Betreibungsämtern</li>
              <li><span class="red">•</span> Mieterscoring für Mieterauswahl</li>
              <li><span class="red">•</span> Schnittstelle zur Auskunfteien </li>
              <li><span class="red">•</span> Online-Mieterportal</li>
              <li><span class="red">•</span> Digitale Schaden- und Auftragsmeldung</li>
              <li><span class="red">•</span> Digitale Abnahme- und Übergabeprotokolle</li>
              <li><span class="red">•</span> Massgeschneidertes Plattform-Design</li>
              <li><span class="red">•</span> Dashboards und Analysen</li>
              <li><span class="red">•</span> Massgeschneiderte Integrationen (REM, Garaio REM, Rimo R4, AbaImmo, HomeGate)</li>
            </ul>
            <div class="price" style="visibility: hidden">
              <p class="red">20</p>
              <p class="price-descr">
                <span class="line-through">30</span> CHF
                <br>
                <span class="red">Monat/Objekt</span>
              </p>
            </div>
            <a href="#" class="button">
              Ihr INDIVIDUELLES Angebot
            </a>
          </div>
        </div>
        <div class="under-preise-flex">
          <p class="under-preise-flex_title">Alle Pakete enthalten:</p>
          <p>
            <span>
              <svg xmlns="http://www.w3.org/2000/svg" width="7.969" height="15" viewBox="0 0 7.969 15">
                <path d="M863,729.989V717.01a0.938,0.938,0,0,1,1.612-.714l6.075,6.49a1.06,1.06,0,0,1,0,1.427l-6.075,6.489A0.938,0.938,0,0,1,863,729.989Z" transform="translate(-863 -716)"/>
              </svg>
              Unbegrenzte Anzahl von Benutzern
            </span>
            <span>
              <svg xmlns="http://www.w3.org/2000/svg" width="7.969" height="15" viewBox="0 0 7.969 15">
                <path d="M863,729.989V717.01a0.938,0.938,0,0,1,1.612-.714l6.075,6.49a1.06,1.06,0,0,1,0,1.427l-6.075,6.489A0.938,0.938,0,0,1,863,729.989Z" transform="translate(-863 -716)"/>
              </svg>
              Import/Export von Daten
            </span>
            <span>
              <svg xmlns="http://www.w3.org/2000/svg" width="7.969" height="15" viewBox="0 0 7.969 15">
                <path d="M863,729.989V717.01a0.938,0.938,0,0,1,1.612-.714l6.075,6.49a1.06,1.06,0,0,1,0,1.427l-6.075,6.489A0.938,0.938,0,0,1,863,729.989Z" transform="translate(-863 -716)"/>
              </svg>
              Telefon-und Mail-Support während der Bürozeiten
            </span>
          </p>
        </div>
      </div>
    </div>
    <div class="form-container">
      <div class="container">
        <div class="flex">
          <div class="form-icon">
            <img src="img/inner-images/form-icon-2.png" alt="icon">
          </div>
          <div class="form">
            <p class="form-title">
              <span class="red">Individuelles</span> Angebot
            </p>
            <p class="form-subtitle">
              Wir freuen uns auf Ihre Anfrage und erstellen Ihnen gerne ein individuelles Angebot.
            </p>
            <div class="form-flex">
              <div>
                <label>
                  <span>Firmenname</span>
                  <input type="text">
                </label>
                <label>
                  <span>Vorname</span>
                  <input type="text">
                </label>
                <label>
                  <span>Nachname</span>
                  <input type="text">
                </label>
                <label>
                  <span>Telefon</span>
                  <input type="tel" placeholder="+38 (XXX) XXX XX XX">
                </label>
              </div>
              <div>
                <label>
                  <span>E-mail</span>
                  <input type="email" placeholder="email@example.com">
                </label>
                <label>
                  <span>Nachricht</span>
                  <textarea></textarea>
                </label>
                <label>
                  <span></span>
                  <a href="#" class="button">Anfrage abschicken</a>
                </label>
              </div>
            </div>
          </div>
        </div>
      </div>
      <?php
        include 'php-components/kostenlos-testen-form.php';
      ?>
    </div>
    <?php
      include 'php-components/contacts.php';
    ?>
  </main>
    
  <?php
    include 'php-components/footer.php';
  ?>
  
  <!-- Scripts -->
    <!-- Libs -->
      <script defer src="js/libs.min.js"></script>
    <!-- Common -->
      <script defer src="js/common.min.js"></script>
    <!-- Custom -->
      <script defer src="js/inner.min.js"></script>
</body>
</html>