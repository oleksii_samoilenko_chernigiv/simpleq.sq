<div id="contacts" class="contacts">
	<div class="container">
		<div class="text-with-left-icon">
			<div class="left-icon">
				<div class="icon-container">
					<img src="img/icons/for-kontakte.svg" alt="icon">
					<p>Kontakte</p>
				</div>
			</div>
			<div class="right-text">
				<p>Haben Sie eine Frage? Interessieren Sie sich für eine Demo? Möchten Sie erfahren, wie Ihr Unternehmen von der Digitalisierung und Analytics profitieren kann?</p>
				<p class="red bold">Wir freuen uns immer, von Ihnen zu hören!</p>
				<ul>
					<li>
						<a href="tel:0041765674063">
							<div class="icon">
								<img src="img/icons/phone.svg" alt="icon">
							</div>
							<p>+41 76 5674063</p>
						</a>
					</li>
					<li>
						<a href="mailto:info@simpleq.ch">
							<div class="icon">
								<img src="img/icons/letter.svg" alt="icon">
							</div>
							<p>info@SimpleQ.ch</p>
						</a>
					</li>
					<li>
						<a href="#">
							<div class="icon">
								<img src="img/icons/marker.svg" alt="icon">
							</div>
							<p>SimpleQ GmbH, Uetlibergstrasse 135, 8045 Zürich, Schweiz</p>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>