<div class="container">
        <div class="flex">
          <div class="form-icon">
            <img src="img/inner-images/form-icon.png" alt="icon">
          </div>
          <div class="form">
            <p class="form-title">
              <span class="red">14 Tage lang</span> SimpleQ Plattform kostenlos testen
            </p>
            <p class="form-subtitle">
              Mit der Testversion können Sie alle* Funktionalitäten der SimpleQ Plattform kostenlos testen und erfahren, wie Sie davon profitieren können.
            </p>
            <div class="form-flex">
              <div>
                <label>
                  <span>Anrede</span>
                  <div class="ui dropdown">
                    <input type="hidden" name="anrede">
                    <div class="default text"></div>
                    <i class="dropdown icon"></i>
                    <div class="menu">
                      <div class="item" data-value="male">Anrede 1</div>
                      <div class="item" data-value="female">Anrede 2</div>
                    </div>
                  </div>
                </label>
                <label>
                  <span>Vorname</span>
                  <input type="text">
                </label>
                <label>
                  <span>Nachname</span>
                  <input type="text">
                </label>
                <label>
                  <span>Position</span>
                  <input type="text">
                </label>
                <label>
                  <span>E-mail</span>
                  <input type="email" placeholder="email@example.com">
                </label>
                <label>
                  <span>Telefon</span>
                  <input type="tel" placeholder="+38 (XXX) XXX XX XX">
                </label>
              </div>
              <div>
                <label>
                  <span>Firmenname</span>
                  <input type="text">
                </label>
                <label>
                  <span class="line-height-min">Anzahl Mitarbeiter</span>
                  <div class="ui dropdown">
                    <input type="hidden" name="anzahl">
                    <div class="default text"></div>
                    <i class="dropdown icon"></i>
                    <div class="menu">
                      <div class="item" data-value="male">Anzahl Mitarbeiter 1</div>
                      <div class="item" data-value="female">Anzahl Mitarbeiter 2</div>
                    </div>
                  </div>
                </label>
                <label>
                  <span>Sprache</span>
                  <div class="ui dropdown">
                    <input type="hidden" name="sprache">
                    <div class="default text"></div>
                    <i class="dropdown icon"></i>
                    <div class="menu">
                      <div class="item" data-value="male">Sprache 1</div>
                      <div class="item" data-value="female">Sprache 2</div>
                    </div>
                  </div>
                </label>
                <label>
                  <span></span>
                  <div class="checkbox-wrapper">
                    <input type="checkbox" checked><div class="checkbox"></div><span class="checkbox-text">Ich stimme dem <a href="#">Abonnementrahmenvertrag</a> zu</span>
                  </div>
                </label>
                <label class="margin-top-6">
                  <span></span>
                  <div class="checkbox-wrapper">
                    <input type="checkbox"><div class="checkbox"></div><span class="checkbox-text">Ja, ich möchte SimpleQ Newsletter erhalten. Ich kann dieses Abonnement jederzeit kündigen</span>
                  </div>
                </label>
                <label>
                  <span></span>
                  <a href="#" class="button">Kostenlos testen</a>
                </label>
                <label>
                  <span></span>
                  <p>* Schnittstelle zu Auskunfteien ausgeschlossen</p>
                </label>
              </div>
            </div>
          </div>
        </div>
      </div>