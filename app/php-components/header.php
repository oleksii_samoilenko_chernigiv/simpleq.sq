<header class="header">
	<a href="../" class="header-logo">
		<img src="img/logo.png" alt="logo">
	</a>
	<div class="header-menu">
		<div class="header-menu-link">
			<p>Immobilienvermietung</p>
			<div class="header-menu-popup">
				<a href="#" class="close-header-menu-popup">
					<svg xmlns="http://www.w3.org/2000/svg" width="9.91" height="6" viewBox="0 0 9.91 6">
					  <path d="M1651.64,48.862l-4.4-4.4a0.547,0.547,0,0,1,0-.768l0.52-.513a0.535,0.535,0,0,1,.76,0l3.51,3.486,3.5-3.486a0.547,0.547,0,0,1,.77,0l0.51,0.513a0.547,0.547,0,0,1,0,.768l-4.4,4.4a0.546,0.546,0,0,1-.77,0h0Z" transform="translate(-1647.09 -43.031)"/>
					</svg>
				</a>
				<a href="#" class="header-menu-popup-link">Funktionen</a>
				<a href="#" class="header-menu-popup-link">Preise</a>
				<a href="#" class="button button-mini button-alt">Kostenlos testen</a>
			</div>
		</div>
		<div class="header-menu-link">
			<p>Immobilienverwaltung</p>
			<div class="header-menu-popup">
				<a href="#" class="close-header-menu-popup">
					<svg xmlns="http://www.w3.org/2000/svg" width="9.91" height="6" viewBox="0 0 9.91 6">
					  <path d="M1651.64,48.862l-4.4-4.4a0.547,0.547,0,0,1,0-.768l0.52-.513a0.535,0.535,0,0,1,.76,0l3.51,3.486,3.5-3.486a0.547,0.547,0,0,1,.77,0l0.51,0.513a0.547,0.547,0,0,1,0,.768l-4.4,4.4a0.546,0.546,0,0,1-.77,0h0Z" transform="translate(-1647.09 -43.031)"/>
					</svg>
				</a>
				<a href="#" class="header-menu-popup-link">Funktionen</a>
				<a href="#" class="header-menu-popup-link">Preise</a>
				<a href="#" class="button button-mini button-alt">Kostenlos testen</a>
			</div>
		</div>
		<div class="header-menu-link">
			<p>Immobilienverkauf</p>
			<div class="header-menu-popup">
				<a href="#" class="close-header-menu-popup">
					<svg xmlns="http://www.w3.org/2000/svg" width="9.91" height="6" viewBox="0 0 9.91 6">
					  <path d="M1651.64,48.862l-4.4-4.4a0.547,0.547,0,0,1,0-.768l0.52-.513a0.535,0.535,0,0,1,.76,0l3.51,3.486,3.5-3.486a0.547,0.547,0,0,1,.77,0l0.51,0.513a0.547,0.547,0,0,1,0,.768l-4.4,4.4a0.546,0.546,0,0,1-.77,0h0Z" transform="translate(-1647.09 -43.031)"/>
					</svg>
				</a>
				<a href="#" class="header-menu-popup-link">Funktionen</a>
				<a href="#" class="header-menu-popup-link">Preise</a>
				<a href="#" class="button button-mini button-alt">Kostenlos testen</a>
			</div>
		</div>
		<a href="#" class="button button-gray header-menu-link">Kostenlos testen</a>
	</div>
	<div class="header-right_menu">
		<div class="ui dropdown header-right_menu-item about_us">
		  <div class="default text">
		  	Über uns
		  	<svg xmlns="http://www.w3.org/2000/svg" width="9.91" height="6" viewBox="0 0 9.91 6">
				  <path d="M1651.64,48.862l-4.4-4.4a0.547,0.547,0,0,1,0-.768l0.52-.513a0.535,0.535,0,0,1,.76,0l3.51,3.486,3.5-3.486a0.547,0.547,0,0,1,.77,0l0.51,0.513a0.547,0.547,0,0,1,0,.768l-4.4,4.4a0.546,0.546,0,0,1-.77,0h0Z" transform="translate(-1647.09 -43.031)"/>
				</svg>
		  </div>
		  <div class="menu">
		    <a href="#cover" class="item">Über SimpleQ</a>
		    <a href="#team" class="item">Team</a>
		    <a href="#slider-block" class="item">Beratungdienstleistungen</a>
		    <a href="#contacts" class="item">Kontakte</a>
		  </div>
		</div>
		<a href="#" class="header-right_menu-item login-button">
			<p>Einloggen</p>
			<svg xmlns="http://www.w3.org/2000/svg" width="13.06" height="10" viewBox="0 0 13.06 10">
			  <path d="M1783.58,52.669h-2.14a0.309,0.309,0,0,1-.31-0.306v-1.02a0.309,0.309,0,0,1,.31-0.306h2.14a0.82,0.82,0,0,0,.82-0.816v-4.9a0.819,0.819,0,0,0-.82-0.816h-2.14a0.309,0.309,0,0,1-.31-0.306V43.18a0.309,0.309,0,0,1,.31-0.306h2.14a2.452,2.452,0,0,1,2.45,2.448v4.9A2.452,2.452,0,0,1,1783.58,52.669Zm-1.2-5.127-4.28-4.285a0.615,0.615,0,0,0-1.05.433v2.449h-3.47a0.608,0.608,0,0,0-.61.612V49.2a0.608,0.608,0,0,0,.61.612h3.47v2.449a0.616,0.616,0,0,0,1.05.434l4.28-4.286A0.612,0.612,0,0,0,1782.38,47.542Z" transform="translate(-1772.97 -42.875)"/>
			</svg>
		</a>
		<div class="ui dropdown header-right_menu-item language">
		  <div class="default text">
		  	De
		  	<img src="img/icons/de.jpg" alt="flag">
		  </div>
		  <div class="menu">
		    <a href="#" class="item">
		    	En
		    	<img src="img/icons/en.jpg" alt="flag">
		    </a>
		    <a href="#" class="item">
		    	It
		    	<img src="img/icons/it.jpg" alt="flag">
		    </a>
		    <a href="#" class="item">
		    	Fr
		    	<img src="img/icons/fr.jpg" alt="flag">
		    </a>
		  </div>
		</div>
	</div>
</header>